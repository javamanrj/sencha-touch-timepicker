Ext.define('Ext.ux.field.TimePicker', {
    extend: 'Ext.field.Text',
    alias : 'widget.timepickerfield',
    requires: [
        'Ext.ux.TimePicker', // put the package of the TimePicker component
        'Ext.DateExtras'
    ],

    /**
     * @event change
     * Fires when a date is selected
     * @param {Ext.field.DatePicker} this
     * @param {Date} date The new date
     */

    config: {
        ui: 'select',
		
        /**
         * @cfg {Object/Dental.view.components.TimePicker} picker
         * An object that is used when creating the internal {@link Dental.view.components.TimePicker} component or a direct instance of {@link Dental.view.components.TimePicker}
         * Defaults to true
         * @accessor
         */
        picker: true,

        /**
         * @cfg
         * @hide
         * @accessor
         */
        clearIcon: false,

        /**
         * @cfg {Object/Date} value
         * Default value for the field and the internal {@link Dental.view.components.TimePicker} component. Accepts an object of 'year',
         * 'month' and 'day' values, all of which should be numbers, or a {@link Date}.
         *
         * Example: {year: 1989, day: 1, month: 5} = 1st May 1989 or new Date()
         * @accessor
         */

        /**
         * @cfg {Boolean} destroyPickerOnHide
         * Whether or not to destroy the picker widget on hide. This save memory if it's not used frequently,
         * but increase delay time on the next show due to re-instantiation. Defaults to false
         * @accessor
         */
        destroyPickerOnHide: false,

        /**
         * @cfg {Number} tabIndex
         * @hide
         */
        tabIndex: -1,

        /**
         * @cfg
         * @hide
         */
        component: {
            useMask: true
        }
    },

    initialize: function() {
        this.callParent(arguments);

        this.getComponent().on({
            scope: this,

            masktap: 'onMaskTap'
        });
    },

    applyValue: function(value) {
        if (!Ext.isDate(value) && !Ext.isObject(value)) {
            value = null;
        }

        if (Ext.isObject(value)) {
        	var tmpDate = new Date();
        	
            value = new  Date(tmpDate.getFullYear(), tmpDate.getMonth(), tmpDate.getDate(), value.hour, value.minute);
        }

        return value;
    },

    // @inherit
    updateValue: function(newValue) {
        var picker = this.getPicker();
        if (this.initialized && picker) {
            picker.setValue(newValue);
        }
        this.getComponent().setValue(Ext.Date.format(newValue, Ext.util.Format.defaultTimeFormat));

        this._value = newValue;
    },

    getValue: function() {
        return this._value;
    },

    /**
     * Returns the value of the field, which will be a {@link Date} unless the <tt>format</tt> parameter is true.
     * @param {Boolean} format True to format the value with <tt>Ext.util.Format.defaultDateFormat</tt>
     */
    getFormattedValue: function(format) {
        var value = this.getValue();
        return (Ext.isDate(value)) ? Ext.Date.format(value, format || Ext.util.Format.defaultTimeFormat) : value;
    },

    applyPicker: function(config) {
        if (!this.initialized) {
            //if this field has not been initialized yet, we don't want to create the picker
            //as it will not be shown immeditely. We will hold this off until the first time
            //it needs to be shown
            return null;
        }
        
        return Ext.factory(config, Dental.view.components.TimePicker, this.getPicker());
    },

    updatePicker: function(newPicker) {
        if (newPicker) {
            newPicker.on({
                scope: this,

                change: 'onPickerChange',
                hide  : 'onPickerHide'
            });

            newPicker.hide();
        }
    },

    /**
     * @private
     * Listener to the tap event of the mask element. Shows the internal {@link #datePicker} component when the button has been tapped.
     */
    onMaskTap: function() {
        if (this.getDisabled()) {
            return false;
        }

        var picker = this.getPicker(),
            initialConfig = this.getInitialConfig();
        
        if (!picker) {
            picker = this.applyPicker(initialConfig.picker);
            this.updatePicker(picker);
            picker.setValue(initialConfig.value);
            this._picker = picker;
        }

        picker.show();

        return false;
    },

    /**
     * Called when the picker changes its value
     * @param {Dental.view.components.TimePicker} picker The date picker
     * @param {Object} value The new value from the date picker
     * @private
     */
    onPickerChange: function(picker, value) {
        var me = this;

        me.setValue(value);
        me.fireAction('change', [me, me.getValue()], 'doChange');
    },

    doChange: Ext.emptyFn,

    /**
     * Destroys the picker when it is hidden, if
     * {@link Ext.field.DatePicker#destroyPickerOnHide destroyPickerOnHide} is set to true
     * @private
     */
    onPickerHide: function() {
        var picker = this.getPicker();

        if (this.getDestroyPickerOnHide() && picker) {
            picker.destroy();
            this.setPicker(null);
        }
    },

    reset: function() {
        this.setValue(this.originalValue);
    },

    // @private
    onDestroy: function() {
        var picker = this.getPicker();
        if (picker) {
            picker.destroy();
        }

        this.callParent(arguments);
    }
}, function() {
    //<deprecated product=touch since=2.0>
    this.override({
        getValue: function(format) {
            if (format) {
                //<debug warn>
                Ext.Logger.deprecate("format argument of the getValue method is deprecated, please use getFormattedValue instead", this);
                //</debug>
                return this.getFormattedValue(format);
            }
            return this.callOverridden();
        }
    });
    //</deprecated>
});