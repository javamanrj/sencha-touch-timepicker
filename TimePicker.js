Ext.define('Ext.ux.TimePicker', {
    extend: 'Ext.picker.Picker',
    xtype: 'timepicker',
    requires: ['Ext.DateExtras'],

    config: {
    	/**
		 * @cfg {Number} minuteScale
		 * List every how many minutes, eg. 5 lists 0, 5, 10, 15, etc. Defaults to 1
		 */
		minuteScale: 1, 
		
        /**
         * @cfg {Number} yearFrom
         * The start year for the date picker.
         * @accessor
         */
        hourFrom: 0,

        /**
         * @cfg {Number} yearTo
         * The last year for the date picker.
         * @default the current year (new Date().getFullYear())
         * @accessor
         */
        hourTo: 23,

        /**
         * @cfg {String} monthText
         * The label to show for the month column.
         * @accessor
         */
        hourText: 'Hour',

        /**
         * @cfg {String} dayText
         * The label to show for the day column.
         * @accessor
         */
        minuteText: 'Minute',

        /**
         * @cfg {Array} slotOrder
         * An array of strings that specifies the order of the slots.
         * @accessor
         */
        slotOrder: ['hour', 'minute']

        /**
         * @cfg {Object/Date} value
         * Default value for the field and the internal {@link Ext.picker.Date} component. Accepts an object of 'year',
         * 'month' and 'day' values, all of which should be numbers, or a {@link Date}.
         *
         * Examples:
         * {year: 1989, day: 1, month: 5} = 1st May 1989.
         * new Date() = current date
         * @accessor
         */
        
        /**
         * @cfg {Boolean} useTitles
         * Generate a title header for each individual slot and use
         * the title configuration of the slot.
         * @accessor
         */

        /**
         * @cfg {Array} slots
         * @hide
         * @accessor
         */
    },

    setValue: function(value, animated) {
        if (Ext.isDate(value)) {
            value = {
                hour  : value.getHours(),
                minute: value.getMinutes(),
            };
        }

        this.callParent([value, animated]);
    },

    getValue: function() {
        var values = {},
            items = this.getItems().items,
            ln = items.length,
            item, i, tmpDate = new Date();

        for (i = 0; i < ln; i++) {
            item = items[i];
            if (item instanceof Ext.picker.Slot) {
                values[item.getName()] = item.getValue();
            }
        }

        return new Date(tmpDate.getFullYear(), tmpDate.getMonth(), tmpDate.getDate(), values.hour, values.minute);
    },

    /**
     * Updates the yearFrom configuration
     */
    updateHourFrom: function() {
        if (this.initialized) {
            this.createSlots();
        }
    },

    /**
     * Updates the yearTo configuration
     */
    updateHourTo: function() {
        if (this.initialized) {
            this.createSlots();
        }
    },
    
    /**
     * Updates the minuteScale configuration
     */
    updateMinuteScale: function() {
    	if (this.initialized) {
    		this.createSlots();
    	}
    },

    /**
     * Updates the minuteText configuration
     */
    updateMinuteText: function(newMinuteText, oldMinuteText) {
        var innerItems = this.getInnerItems,
            ln = innerItems.length,
            item, i;
        
        //loop through each of the current items and set the title on the correct slice
        if (this.initialized) {
            for (i = 0; i < ln; i++) {
                item = innerItems[i];

                if ((typeof item.title == "string" && item.title == oldMinuteText) || (item.title.html == oldMinuteText)) {
                    item.setTitle(newMinuteText);
                }
            }
        }
    },

    /**
     * Updates the hourText configuraton
     */
    updateHourText: function(newHourText, oldHourText) {
        var innerItems = this.getInnerItems,
            ln = innerItems.length,
            item, i;

        //loop through each of the current items and set the title on the correct slice
        if (this.initialized) {
            for (i = 0; i < ln; i++) {
                item = innerItems[i];

                if ((typeof item.title == "string" && item.title == oldHourText) || (item.title.html == oldHourText)) {
                    item.setTitle(newHourText);
                }
            }
        }
    },

    // @private
    constructor: function() {
        this.callParent(arguments);
        this.createSlots();
    },

    /**
     * Generates all slots for all years specified by this component, and then sets them on the component
     * @private
     */
    createSlots: function() {
        var me        = this,
            slotOrder = this.getSlotOrder(),
            hoursFrom = me.getHourFrom(),
            hoursTo   = me.getHourTo(),
            minuteScale = me.getMinuteScale(),
            hours     = [],
            minutes    = [],
            tmp, i;
        
        // swap values if user mixes them up.
        if (hoursFrom > hoursTo) {
            tmp = hoursFrom;
            hoursFrom = hoursTo;
            hoursTo = tmp;
        }

        for (i = hoursFrom; i <= hoursTo; i++) {
        	hours.push({
                text: i,
                value: i
            });
        }

        for (i = j = 0; i <= 59; i = j = i + minuteScale) {
			j = (j+"").length > 1 ? j : "0"+j;
            minutes.push({
                text: j,
                value: i
            });
        }

        var slots = [];

        slotOrder.forEach(function(item) {
            slots.push(this.createSlot(item, hours, minutes));
        }, this);

        me.setSlots(slots);
    },

    /**
     * Returns a slot config for a specified date.
     * @private
     */
    createSlot: function(name, hours, minutes) {
        switch (name) {
            case 'hour':
                return {
                    name: name,
                    align: 'right',
                    data: hours,
                    title: this.getHourText(),
                    flex: 5
                };
            case 'minute':
                return {
                    name: name,
                    align: 'left',
                    data: minutes,
                    title: this.getMinuteText(),
                    flex: 5
                };
        }
    }
});
